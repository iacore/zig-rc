const std = @import("std");
const Builder = std.build.Builder;

/// use this function to use this library
/// see `build` below
pub fn package() std.build.Pkg {
    const dirname = comptime std.fs.path.dirname(@src().file).?;
    return .{
        .name = "rc",
        .source = .{ .path = dirname ++ std.fs.path.sep_str ++ "lib.zig" },
    };
}

pub fn build(b: *Builder) void {
    const mode = b.standardReleaseOptions();

    var main_tests = b.addTest("test.zig");
    main_tests.setBuildMode(mode);
    main_tests.addPackage(package()); // include package like this

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
}
