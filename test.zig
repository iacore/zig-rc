const std = @import("std");
const testing = std.testing;
const lib = @import("rc");


const Atomic = lib.RealAtomic;

test "atomic increment" {
    var val: usize = 0;
    try testing.expectEqual(@intCast(usize, 0), Atomic.increment(&val));
    try testing.expectEqual(@intCast(usize, 1), Atomic.load(&val));
}

test "clamped atomic increment" {
    var val: usize = 0;
    try testing.expectEqual(@intCast(usize, 0), Atomic.clampedIncrement(&val));
    try testing.expectEqual(@intCast(usize, 0), Atomic.load(&val));
}

test "saturating atomic increment from max usize" {
    var val: usize = std.math.maxInt(usize);
    try testing.expectEqual(@intCast(usize, std.math.maxInt(usize)), Atomic.increment(&val));
    try testing.expectEqual(@intCast(usize, std.math.maxInt(usize)), Atomic.load(&val));
}




fn increment100(val: *usize) void {
    var i: usize = 0;
    while (i < 100) {
        // this is to ensure the thread doesn't finish too early
        std.time.sleep(10 * std.time.ns_per_ms);
        _ = Atomic.increment(val);
        i += 1;
    }
}

fn decrement100(val: *usize) void {
    var i: usize = 0;
    while (i < 100) {
        // this is to ensure the thread doesn't finish too early
        std.time.sleep(10 * std.time.ns_per_ms);
        _ = Atomic.decrement(val);
        i += 1;
    }
}

test "concurrent atomic increments & decrements" {
    var val: usize = 0;
    const t1 = try std.Thread.spawn(.{}, increment100, .{&val});
    const t2 = try std.Thread.spawn(.{}, increment100, .{&val});
    t1.join();
    t2.join();
    try testing.expectEqual(@intCast(usize, 200), Atomic.load(&val));
    const t3 = try std.Thread.spawn(.{}, decrement100, .{&val});
    const t4 = try std.Thread.spawn(.{}, decrement100, .{&val});
    t3.join();
    t4.join();
    try testing.expectEqual(@intCast(usize, 0), Atomic.load(&val));
}

test "saturating concurrent atomic increments" {
    var val: usize = std.math.maxInt(usize) - 100;
    const t1 = try std.Thread.spawn(.{}, increment100, .{&val});
    const t2 = try std.Thread.spawn(.{}, increment100, .{&val});
    t1.join();
    t2.join();
    try testing.expectEqual(@intCast(usize, std.math.maxInt(usize)), Atomic.load(&val));
}

test "atomic decrement" {
    var val: usize = 1;
    try testing.expectEqual(Atomic.decrement(&val), 1);
    try testing.expectEqual(Atomic.load(&val), 0);
}

test "saturating atomic decrement from 0" {
    var val: usize = 0;
    try testing.expectEqual(Atomic.decrement(&val), 0);
    try testing.expectEqual(Atomic.load(&val), 0);
}

test "saturating concurrent atomic decrements" {
    var val: usize = std.math.minInt(usize) + 100;
    const t1 = try std.Thread.spawn(.{}, decrement100, .{&val});
    const t2 = try std.Thread.spawn(.{}, decrement100, .{&val});
    t1.join();
    t2.join();
    try testing.expectEqual(@intCast(usize, std.math.minInt(usize)), Atomic.load(&val));
}


const NonAtomic = lib.NonAtomic;

test "non-atomic increment" {
    var val: usize = 0;
    try testing.expectEqual(NonAtomic.increment(&val), 0);
    try testing.expectEqual(NonAtomic.load(&val), 1);
}

test "clamped non-atomic increment" {
    var val: usize = 0;
    try testing.expectEqual(@intCast(usize, 0), NonAtomic.clampedIncrement(&val));
    try testing.expectEqual(@intCast(usize, 0), NonAtomic.load(&val));
}

test "non-atomic increment from max usize" {
    var val: usize = std.math.maxInt(usize);
    try testing.expectEqual(NonAtomic.increment(&val), std.math.maxInt(usize));
    try testing.expectEqual(NonAtomic.load(&val), std.math.maxInt(usize));
}

test "non-atomic decrement" {
    var val: usize = 1;
    try testing.expectEqual(NonAtomic.decrement(&val), 1);
    try testing.expectEqual(NonAtomic.load(&val), 0);
}

test "non-atomic decrement from 0" {
    var val: usize = 0;
    try testing.expectEqual(NonAtomic.decrement(&val), 0);
    try testing.expectEqual(NonAtomic.load(&val), 0);
}



const RcSharedPointer = lib.RcSharedPointer;

test "initializing a shared pointer with a value" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    defer _ = v.deinit();
    try testing.expectEqual(v.ptr().*, 10);
}

test "unsafely mutating shared pointer's value" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    defer _ = v.deinit();
    const ptr = v.unsafePtr();
    ptr.* = 20;
    try testing.expectEqual(v.ptr().*, 20);
}

test "safely mutating shared pointer's value" {
    const MU128 = struct {
        value: u128,
        mutex: std.Thread.Mutex = .{},
    };
    var mutex = MU128{ .value = 10 };
    var v = try RcSharedPointer(MU128, NonAtomic).init(mutex, testing.allocator);
    defer _ = v.deinit();
    const ptr = v.unsafePtr();
    {
        ptr.mutex.lock();
        defer ptr.mutex.unlock();
        ptr.value = 20;
    }
}

fn deinit_copy(val: *u128, ctx: *u128) void {
    ctx.* = val.*;
}
test "deinitializing a shared pointer with a callback" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    var v1: u128 = 0;
    _ = v.deinitWithCallback(*u128, &v1, deinit_copy);
    try testing.expectEqual(v1, 10);
}

test "strong-cloning a shared pointer" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    defer _ = v.deinit();
    try testing.expectEqual(@intCast(usize, 1), v.strongCount());
    var v1 = v.strongClone();
    try testing.expectEqual(@intCast(usize, 2), v.strongCount());
    _ = v1.deinit();
    try testing.expectEqual(@intCast(usize, 1), v.strongCount());
}

fn deinit_incr(val: *u128, ctx: *u128) void {
    ctx.* += val.*;
}
test "deinitializing a shared pointer with a callback and strong copies" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    var r: u128 = 0;
    var v1 = v.strongClone();
    try testing.expectEqual(false, v.deinitWithCallback(*u128, &r, deinit_incr));
    try testing.expectEqual(true, v1.deinitWithCallback(*u128, &r, deinit_incr));
    try testing.expectEqual(r, 10); // not 20 because the callback should only be called once
}

test "weak-cloning a shared pointer" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    defer _ = v.deinit();
    try testing.expectEqual(@intCast(usize, 0), v.weakCount());
    var v1 = v.weakClone();
    try testing.expectEqual(@intCast(usize, 1), v.weakCount());
    _ = v1.deinit();
    try testing.expectEqual(@intCast(usize, 0), v.weakCount());
}

test "weak-cloning a shared pointer" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    defer _ = v.deinit();
    try testing.expectEqual(@intCast(usize, 0), v.weakCount());
    var v1 = v.weakClone();
    try testing.expectEqual(@intCast(usize, 1), v.weakCount());
    var v2 = v.weakClone();
    try testing.expectEqual(@intCast(usize, 2), v.weakCount());
    _ = v1.deinit();
    _ = v2.deinit();
    try testing.expectEqual(@intCast(usize, 0), v.weakCount());
}

test "strong-cloning a weak clone" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    defer _ = v.deinit();
    try testing.expectEqual(@intCast(usize, 0), v.weakCount());
    var v1 = v.weakClone();
    try testing.expectEqual(@intCast(usize, 1), v.weakCount());
    var v2 = v1.strongClone().?;
    defer _ = v2.deinit();
    _ = v1.deinit();
    try testing.expectEqual(@intCast(usize, 0), v.weakCount());
    try testing.expectEqual(@intCast(usize, 2), v.strongCount());
}

test "strong-cloning a weak clone with no other strong clones" {
    var v = try RcSharedPointer(u128, NonAtomic).init(10, testing.allocator);
    var v1 = v.weakClone();
    try testing.expectEqual(@intCast(usize, 1), v.weakCount());
    _ = v.deinit();
    try testing.expectEqual(@intCast(usize, 0), v1.strongCount());
    try testing.expectEqual(v1.strongClone(), null);
    _ = v1.deinit();
}


// change this
pub const test_race_condition_retry_times = 10000;


var deinit50c: usize = 0;
var deinit50s = false;

fn deinit_incr50(val: *u128, ctx: *usize) void {
    _ = val;
    ctx.* += 1;
}

fn deinit50(val: *[50]RcSharedPointer(u128, Atomic)) void {
    // std.testing.log_level = .info;
    var i: usize = 0;
    var r: usize = 0;
    // const tid = std.Thread.getCurrentId();
    // std.log.info("tid={} start", .{tid});
    // wait until a signal to go is given
    while (!@atomicLoad(bool, &deinit50s, .Acquire)) {}
    // std.log.info("tid={} after block", .{tid});
    while (i < 50) : (i += 1) {
        // std.log.info("i={} tid={}", .{i, std.Thread.getCurrentId()});
        _ = val[i].deinitWithCallback(*usize, &r, deinit_incr50);
    }
    _ = @atomicRmw(usize, &deinit50c, .Add, @intCast(usize, r), .SeqCst);
}

// the idea here is to try and cause a race condition deinitializing
// strong clones (only they deinitialize)
test "deinitializing atomic reference counter" {
    const T = RcSharedPointer(u128, Atomic);
    var c: usize = 0;
    // try this many times
    while (c < test_race_condition_retry_times) : (c += 1) {
        @atomicStore(usize, &deinit50c, 0, .SeqCst);
        var v = try T.init(10, testing.allocator);
        var i: usize = 0;
        var clones = try std.ArrayList(T).initCapacity(testing.allocator, 150);
        defer clones.deinit();
        try clones.append(v);
        while (i < 149) : (i += 1) {
            try clones.append(v.strongClone());
        }

        // race three threads to deinitialize
        @atomicStore(bool, &deinit50s, false, .SeqCst);
        const t1 = try std.Thread.spawn(.{}, deinit50, .{clones.items[0..50]});
        const t2 = try std.Thread.spawn(.{}, deinit50, .{clones.items[50..100]});
        const t3 = try std.Thread.spawn(.{}, deinit50, .{clones.items[100..150]});
        @atomicStore(bool, &deinit50s, true, .SeqCst);
        t1.join();
        t2.join();
        t3.join();

        // ensure that we only deinitialized once
        try testing.expectEqual(@intCast(usize, 1), @atomicLoad(usize, &deinit50c, .SeqCst));
    }
}

var dealloc50c: usize = 0;

fn deinit50_alloc(val: *[50]RcSharedPointer(u128, Atomic)) void {
    var i: usize = 0;
    // wait until a signal to go is given
    while (!@atomicLoad(bool, &deinit50s, .SeqCst)) {}
    while (i < 50) : (i += 1) {
        if (val[i].deinit()) {
            _ = @atomicRmw(usize, &dealloc50c, .Add, @intCast(usize, 1), .SeqCst);
        }
    }
}

// weak version
fn deinit50w_alloc(val: *[50]RcSharedPointer(u128, Atomic).Weak) void {
    var i: usize = 0;
    // wait until a signal to go is given
    while (!@atomicLoad(bool, &deinit50s, .SeqCst)) {}
    while (i < 50) : (i += 1) {
        if (val[i].deinit()) {
            _ = @atomicRmw(usize, &dealloc50c, .Add, @intCast(usize, 1), .SeqCst);
        }
    }
}

// the idea here is to try and cause a race condition deallocating
// strong and weak clones
test "deallocating atomic reference counter" {
    const T = RcSharedPointer(u128, Atomic);
    var c: usize = 0;
    // try this many times
    while (c < test_race_condition_retry_times) : (c += 1) {
        @atomicStore(usize, &dealloc50c, 0, .SeqCst);
        var v = try T.init(10, testing.allocator);
        var i: usize = 0;
        var clones = try std.ArrayList(T).initCapacity(testing.allocator, 100);
        defer clones.deinit();
        try clones.append(v);
        while (i < 99) : (i += 1) {
            try clones.append(v.strongClone());
        }

        var wclones = try std.ArrayList(T.Weak).initCapacity(testing.allocator, 50);
        defer wclones.deinit();
        i = 0;
        while (i < 50) : (i += 1) {
            try wclones.append(v.weakClone());
        }

        // race three threads to deallocate
        @atomicStore(bool, &deinit50s, false, .SeqCst);
        const t1 = try std.Thread.spawn(.{}, deinit50_alloc, .{clones.items[0..50]});
        const t2 = try std.Thread.spawn(.{}, deinit50_alloc, .{clones.items[50..100]});
        const t3 = try std.Thread.spawn(.{}, deinit50w_alloc, .{wclones.items[0..50]});
        @atomicStore(bool, &deinit50s, true, .SeqCst);
        t1.join();
        t2.join();
        t3.join();

        // ensure that we only deallocated once
        try testing.expectEqual(@intCast(usize, 1), @atomicLoad(usize, &dealloc50c, .SeqCst));
    }
}
