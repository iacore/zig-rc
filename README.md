# Reference-counted Shared Pointer for Zig

This is an implementation of (both atomically and non-atomically) reference-counted shared pointer for [Zig](https://ziglang.org). It's somewhat similar to Rust's [Rc](https://doc.rust-lang.org/std/rc/struct.Rc.html) and [Arc](https://doc.rust-lang.org/std/sync/struct.Arc.html) as well as C++'s [shared_ptr](https://en.cppreference.com/w/cpp/memory/shared_ptr).

It's in early stages of development and is **not** proven to be correct <del>or feature-complete</del>. Let's call it a prototype.

## Dumpster diving notes

I found this library abandoned at https://github.com/yrashk/zig-rcsp.git. I made it work with Zig 0.10 and 0.11-dev. The code is still sketchy.

I recommend bohem-gc if your project has enough time budget. Some precise GC may be batter. You may also want to use C++ or Rust, lest you forget to inc/dec your RC pointers.

To use this library, do

```
exe.addPackage(@import("zig-rcsp/build.zig").package());
```
